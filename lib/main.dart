import 'package:flutter/material.dart';
import 'otherpage.dart';

void main() {
  runApp(new MaterialApp(home: new application()));
}

class application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Drawer'),),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                accountName: new Text('My Name'),
                accountEmail: new Text('myemail@email.com'),
                currentAccountPicture: new CircleAvatar(
                  backgroundColor: Colors.black,
                  child: new Text('J'),),
            ),
            new ListTile(title: new Text('Thing 1'),
              trailing: new Icon(Icons.arrow_forward),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new op('page 1'))),
            ),
            new ListTile(title: new Text('Thing 2'),
              trailing: new Icon(Icons.arrow_forward),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new op('page 2'))),),
            new ListTile(title: new Text('Close'),
              trailing: new Icon(Icons.close),
              onTap: () {Navigator.pop(context);},),
          ],
        ),
      ),
    );
  }
}
